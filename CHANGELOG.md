This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# Changelog for "wps"


## [v1.5.0]

- Updated gcube-smartgears-bom to 2.5.1


## [v1.4.0] - 2022-12-06

- Updated list parameter [#24026]
- Fixed Bionym Local problem [#24209]


## [v1.3.0] - 2022-08-26

- Updated service to common-gcube-calls-1.3.1 [#23769]


## [v1.2.0] - 2022-04-05

-  Added support to  new JWT token via URI Resolver [#23107]


## [v1.1.10] - 2022-03-21

- Update wps service to support not writing of the computation status to the user's workspace [#23054]
- Fixed protocol parameter when persistence is disabled 


## [v1.1.9] - 2022-01-26

- Updated pom.xml to fix max computation limit [#22700]


## [v1.1.8] - 2021-05-17

- Updated pom.xml to add to the war the new libraries released to solve slf4j-over-log4j issue [#11956]


## [v1.1.7] - 2020-11-11

 - Added a printStackTrace in executeRequest
 - config Path configurable via web.xml
 - fix repository declarations


## [v1.1.6] - 2020-10-15

- Updated pom.xml for support gcube-bom-2.0.0-SNAPSHOT [#19790]


## [v1.1.5] - 2020-06-05

- Updated ExecuteResponseBuilder for support protocol option [#19423]
- Updated to Git and Jenkins [#18120]


## [v1.1.3] - 2018-12-13

- Added https support [#13024]


## [v1.1.0] - 2017-09-24

- Dynamic adding, removing and updating of algorithms added
- Moved to SocialNetworkingService 2.0


## [v1.0.0] - 2017-09-01

- First Release

